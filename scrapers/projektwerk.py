import requests
from bs4 import BeautifulSoup
from .config import projektwerk
from threading import Thread
from queue import Queue
import time
import random
import csv

def create_session(proxy_list):
    for x in range(100):
        try:
            selected_proxy = random.choice(proxy_list)
            proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            session = requests.Session()
            resp = session.get('https://www.projektwerk.com/de/login', proxies=proxy, timeout=(20,30))
            soup = BeautifulSoup(resp.content, 'lxml')
            form = soup.find('form', {'name': 'user_login'})
            params = {}
            inputs = form.find_all('input')
            for input in inputs:
                params[input.get('name')] = input.get('value')
            params['user_login[email]'] = projektwerk['username']
            params['user_login[password]'] = projektwerk['pwd']
            resp = session.post('https://ww.projektwerk.com/de/login_check', params, proxies=proxy, timeout=(20,30))
            if resp.status_code == 302:
                return (session, proxy)
        except:
            pass
    else:
        return False



def scraper1(queue_in, proxy_list):
    print('scraper1 started')
    session, proxy = create_session(proxy_list)
    if session:
        url = 'https://www.projektwerk.com/de/freelancer?limit=20&page={}'
        page = 1
        while True:
            try:
                resp = session.get(url.format(page), proxies=proxy, timeout=(20,30))
                if not resp.status_code == 200:
                    break
                soup = BeautifulSoup(resp.content, 'lxml')
                freelancer_list = soup.find_all('article')
                counter = 0
                for freelancer in freelancer_list:
                    free_div = freelancer.find('div', {'class':'title'})
                    free_header = free_div.find('h2')
                    free_link = free_header('a')
                    row = {'profile': free_link.get_attribute('href'),
                           'id':free_link.get_attribute('href').split('/')[-1].split('-')[0]}
                    queue_in.put_nowait({'profile_id':id.split('object_id_')[1]})
                    counter +=1
                if counter > 0:
                    page +=1
                    print(page)
                    time.sleep(3)
                else:
                    queue_in.put_nowait('##COMPLETED##')
                    break
            except:
                queue_in.put_nowait('##COMPLETED##')
                break


def scraper2(queue_in, queue_out, proxy_list):
    print('scraper2 started')
    session, proxy = create_session(proxy_list)
    new_data = []
    url = 'https://www.freelance.de/Freiberufler/{}'
    if session:
        while True:
            new_row = queue_in.get()
            if '##COMPLETED##' in new_row:
                queue_out.put_nowait('##COMPLETED2##')
                break
            try:
                for x in range(100):
                    try:
                        selected_proxy = random.choice(proxy_list)
                        proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
                        resp = session.get(url.format(new_row['profile_id']), proxies=proxy, timeout=20)
                        if resp.status_code == 200:
                            new_row['profile'] = resp.url
                            soup = BeautifulSoup(resp.content, 'lxml')
                            profile = soup.find('div', {'id':'profile_container'})
                            profile_panel = profile.find('div', {'id':'profile_view'})
                            title = profile_panel.find('h1')
                            new_row['title'] = title.text
                            panels = soup.find_all('div', {'class': 'panel-default'})
                            skills_list = []
                            for panel in panels:
                                h3 = panel.find('h3')
                                if h3:
                                    if 'ich biete' in h3.get_text().lower():
                                        skill1 = panel.find('div', {'class': 'panel-body'})
                                        skill3 = skill1.find_all('div', {'class':'col-md-8'})
                                        for skill in skill3:
                                            new_skill = skill.get_text().strip()
                                            if len(new_skill) > 0:
                                                skills_list.append(new_skill)
                            new_row['skills'] = ",".join(skills_list)
                        params = {'action':'count_shown_profile_contact_data','profile_id':'130661'}
                        resp = session.get('https://www.freelance.de/profile_freelancer/index.php',
                                           params=params, proxies=proxy, timeout=20)
                        data = resp.json()
                        soup = BeautifulSoup(data['result'], 'lxml')
                        rows = soup.find_all('div', {'class': 'row'})
                        for row in rows:
                            index = row.find('strong').get_text().lower()
                            if index:
                                data_item = row.find('div', {'class': 'col-xs-8'}).get_text()
                                new_row[index] = data_item
                        queue_out.put_nowait(new_row)
                        break
                    except:
                        pass
            except Exception as e:
                print(e)

def controller():
    with open('scrapers/proxies.csv', 'r') as f:
        proxy_list = [x for x in csv.DictReader(f)]
        queue_in = Queue()
        queue_out = Queue()
        th1 = Thread(target=scraper1, args=[queue_in, proxy_list])
        th1.start()
        time.sleep(2)
        th2 = Thread(target=scraper2, args=[queue_in, queue_out, proxy_list])
        th2.start()
        th3 = Thread(target=scraper2, args=[queue_in, queue_out, proxy_list])
        th3.start()
        data = []
        while True:
            rec = queue_out.get()
            print(rec)
            if '##COMPLETED2##' in rec:
                break
            else:
                data.append(rec)
        return data
