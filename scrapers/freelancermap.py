import requests
from bs4 import BeautifulSoup
from .config import freelancermap
import random
import time
import csv
from threading import Thread
from queue import Queue

def create_session(proxy_list):
    for x in range(100):
        try:
            selected_proxy = random.choice(proxy_list)
            proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
            session = requests.Session()
            resp = session.get('https://www.freelancermap.de/login', proxies=proxy, timeout=25)
            soup = BeautifulSoup(resp.content, 'lxml')
            form = soup.find('form', {'id': 'loginform'})
            params = {'login':freelancermap['username'], 'password':freelancermap['password']}
            resp = session.post('https://www.freelancemap.de/login',
                                params, proxies=proxy, timeout=25)
            if "bojan momtschilow" in str(resp.content).lower():
                return session
                break
        except:
            pass
    else:
        return False



def scraper1(queue_in, proxy_list):
    print('scraper1 started')
    session, proxy = create_session(proxy_list)
    if session:
        url = 'https://www.freelancermap.de/?module=freelancer&func=suchergebnisse&qall=&qbez=&qname=&qort=&qskills=&qver=&qref=&qstdmin=&qstdmax=&qtagmin=&qtagmax=&qsort=&qplz=&qumkreis=&qanlagen=&qvideos=&qavailability=&area=newfv&profisuche=&q_id_plattform=1&amazon_plattform=1&dynasearch=&subCats=&mCats=&permanent_jobs=&id_plattform=1&redirect=1'
        while True:
            try:
                resp = session.get(url, proxies=proxy, timeout=25)
                if not resp.status_code == 200:
                    print('break1')
                    break
                soup = BeautifulSoup(resp.content, 'lxml')
                freelancer_section = soup.find('form', {'name':'merklisteform'})
                freelancers = freelancer_section.find_all('li', {'class':'freelancer-row'})
                for freelancer in freelancers:
                    id = freelancer.get('data-id')
                    profile = freelancer.find.a.href
                    title = freelancer.h3.a.get_text()
                    queue_in.put_nowait({'id':id, 'profile':profile, 'title':title})
                try:
                    next_page=False
                    pages = soup.find('div', {'class':'pagination'})
                    new_pages = pages.find_all('a', {'class':'next'})
                    for page in new_pages:
                        if '>>' in page.get_text().lower():
                            url = page.get_attribute('href')
                            next_page=True
                    if not next_page:
                        queue_in.put_nowait('##COMPLETED##')
                        break
                except:
                    queue_in.put_nowait('##COMPLETED##')
                    break
            except:
                break
    queue_in.put_nowait('##COMPLETED##')

def scraper2(queue_in, queue_out, proxy_list):
    print('scraper2 started')
    session, proxy = create_session(proxy_list)
    if session:
        while True:
            new_row = queue_in.get()
            if '##COMPLETED##' in new_row:
                queue_out.put_nowait('##COMPLETED2##')
                break
            try:
                for x in range(100):
                    try:
                        selected_proxy = random.choice(proxy_list)
                        proxy = {'https': 'https://{}:{}'.format(selected_proxy['ip'], selected_proxy['port'])}
                        resp = session.get(new_row['profile'], proxies=proxy, timeout=20)
                        if resp.status_code == 200:
                            soup = BeautifulSoup(resp.content, 'lxml')
                            freelancer_head = soup.find('div', {'id':'freelancer-head'})
                            head_details = freelancer_head.find('div', {'class':'col-md-9'})
                            try:
                                new_row['name'] = head_details.find('li', {'class':'m-b-1'}).get_text()
                            except:
                                new_row['name'] =''
                            address_details = head_details.find('div', {'id':'profil_address'})
                            new_row['address'] = " ".join([x.get_text() for x in address_details.find_all('span')])
                            try:
                                phone_details = freelancer_head.find('div', {'id':'profile_userdata_phone'})
                                new_row['phone'] = phone_details.find('div').get_text()
                            except:
                                new_row['phone'] = ''
                            try:
                                email_details = freelancer_head.find('div', {'id':'profile_userdata_email'})
                                new_row['email'] = email_details.find('div').get_text()
                            except:
                                new_row['email'] = ''

                            new_row['skills'] = soup.find('div',{'data-translatable':'skills'}).get_text()
                            queue_out.put_nowait(new_row)
                            break
                    except:
                        pass
            except:
                queue_out.put_nowait("##COMPLETED##")
                pass
    queue_out.put_nowait("##COMPLETED##")

def controller():
    with open('scraper/proxies.csv', 'r') as f:
        proxy_list = [x for x in csv.DictReader(f)]
        queue_in = Queue()
        queue_out = Queue()
        th1 = Thread(target=scraper1, args=[queue_in, proxy_list])
        th1.start()
        time.sleep(2)
        th2 = Thread(target=scraper2, args=[queue_in, queue_out, proxy_list])
        th2.start()
        th3 = Thread(target=scraper2, args=[queue_in, queue_out, proxy_list])
        th3.start()
        data = []
        while True:
            rec = queue_out.get()
            print(rec)
            if '##COMPLETED##' in rec:
                break
            else:
                data.append(rec)
        return data

